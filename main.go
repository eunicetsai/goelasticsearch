package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var (
	// Basic information for the Amazon Elasticsearch Service domain
	domain = "https://search-revex-es-test-5bhjncn62ygc4bknxhvjkngale.us-east-1.es.amazonaws.com" // e.g. https://my-domain.region.es.amazonaws.com
	index  = "provider"
)

func main() {
	Get()
	// Put()
}

func Get() {
	id := "1"
	endpoint := domain + "/" + index + "/" + "_doc" + "/" + id

	// An HTTP client for sending the request
	client := &http.Client{}

	// Form the HTTP request
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)

	// Set BasicAuth
	req.SetBasicAuth("test", "1qaz!QAZ")

	if err != nil {
		fmt.Print(err)
	}

	// You can probably infer Content-Type programmatically, but here, we just say that it's JSON
	req.Header.Add("Content-Type", "application/json")

	//Get response
	resp, err := client.Do(req)
	if err != nil {
		fmt.Print(err)
	}

	// Print out resposne
	fmt.Print(resp.Status + "\n")
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	bodyString := string(bodyBytes)
	fmt.Print(bodyString)
}

//Update
func Put() {
	id := "1"
	endpoint := domain + "/" + index + "/" + "_doc" + "/" + id
	// service := "es"

	// Sample JSON document to be included as the request body
	json := `{ "name": "Eunice", "age": 20}`
	body := strings.NewReader(json)

	// Get credentials from environment variables and create the AWS Signature Version 4 signer
	// credentials := credentials.NewEnvCredentials()
	// signer := v4.NewSigner(credentials)

	// An HTTP client for sending the request
	client := &http.Client{}

	// Form the HTTP request
	req, err := http.NewRequest(http.MethodPut, endpoint, body)

	// Set BasicAuth
	req.SetBasicAuth("test", "1qaz!QAZ")

	if err != nil {
		fmt.Print(err)
	}

	// You can probably infer Content-Type programmatically, but here, we just say that it's JSON
	req.Header.Add("Content-Type", "application/json")

	// Sign the request, send it, and print the response
	// signer.Sign(req, body, service, region, time.Now())

	resp, err := client.Do(req)
	if err != nil {
		fmt.Print(err)
	}
	fmt.Print(resp.Status + "\n")
}

// package main

// import (
// 	"context"
// 	"fmt"

// 	"github.com/olivere/elastic/v7"
// )

// // Elasticsearch demo

// type Person struct {
// 	Name    string `json:"name"`
// 	Age     int    `json:"age"`
// 	Married bool   `json:"married"`
// }

// func main() {
// 	client, err := elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"))
// 	if err != nil {
// 		// Handle error
// 		panic(err)
// 	}

// 	fmt.Println("connect to es success")
// 	p1 := Person{Name: "lmh", Age: 18, Married: false}
// 	put1, err := client.Index().
// 		Index("user").
// 		BodyJson(p1).
// 		Do(context.Background())
// 	if err != nil {
// 		// Handle error
// 		panic(err)
// 	}
// 	fmt.Printf("Indexed user %s to index %s, type %s\n", put1.Id, put1.Index, put1.Type)
// }
