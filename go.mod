module m

go 1.14

require (
	github.com/aws/aws-sdk-go v1.33.5
	github.com/olivere/elastic/v7 v7.0.19
)
